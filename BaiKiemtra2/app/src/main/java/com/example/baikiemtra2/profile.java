package com.example.baikiemtra2;

public class profile {
    private String title;
    private int hinh;

    public profile(String title, int hinh) {
        this.title = title;
        this.hinh = hinh;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getHinh() {
        return hinh;
    }

    public void setHinh(int hinh) {
        this.hinh = hinh;
    }
}
