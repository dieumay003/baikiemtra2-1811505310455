package com.example.baikiemtra2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<profile> profileArrayList;
    profileadapter profileadapter;
    int vitri =-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
        profileadapter = new profileadapter(this,R.layout.listlayout,profileArrayList);
        listView.setAdapter(profileadapter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        registerForContextMenu(listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                vitri = position;
                return false;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, detail.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search,menu);
        return true;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.contextmenu, menu);
        menu.setHeaderTitle("Chọn các tác vụ");
        menu.setHeaderIcon(R.mipmap.ic_launcher);
        super.onCreateContextMenu(menu, v, menuInfo);
    }
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.xoa:
                xacnhan(vitri);
                break;
        }
        return super.onContextItemSelected(item);
    }
    private void xacnhan(final int vt){
        AlertDialog.Builder aler = new AlertDialog.Builder(this);
        aler.setTitle("Thông Báo!");
        aler.setMessage("Bạn có chắc chắn xóa?");
        aler.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                profileArrayList.remove(vt);
                profileadapter.notifyDataSetChanged();
            }
        });
        aler.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        aler.setIcon(R.mipmap.ic_launcher);
        aler.show();
    }
    private void anhxa(){
        listView = (ListView) findViewById(R.id.lvpr);
        profileArrayList = new ArrayList<>();
        profileArrayList.add(new profile("Arryly", R.drawable.chandung1));
        profileArrayList.add(new profile("ThanhDuy",R.drawable.chandung2));
        profileArrayList.add(new profile("Belo",R.drawable.chandung3));
        profileArrayList.add(new profile("ThanhDuy",R.drawable.chandung4));
        profileArrayList.add(new profile("Xelala",R.drawable.chandung5));
        profileArrayList.add(new profile("Nguyen",R.drawable.chandung6));
    }
}