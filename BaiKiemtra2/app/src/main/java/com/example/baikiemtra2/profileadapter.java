package com.example.baikiemtra2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class profileadapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<profile> profileList;

    public profileadapter(Context context, int layout, List<profile> profileList) {
        this.context = context;
        this.layout = layout;
        this.profileList = profileList;
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder{
        ImageView imghinh;
        TextView txttitle;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layout,null);
            // anh xạ view
            holder = new ViewHolder();
            holder.txttitle = (TextView) convertView.findViewById(R.id.titlet);
            holder.imghinh =  (ImageView) convertView.findViewById(R.id.hinh);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        // lay gia trị
        profile pr = profileList.get(position);
        holder.txttitle.setText(pr.getTitle());
        holder.imghinh.setImageResource(pr.getHinh());
        return convertView;
    }
}
